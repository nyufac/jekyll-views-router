module Jekyll
  class Generator

    def to_slug
      self.class.name
        .split('::')
        .last
        .gsub(/(.)([A-Z])/,'\1_\2')
        .downcase
    end

    def views(site)
      site
        .pages
        .select { |page| (page.data['uses'] || []).include? self.to_slug }
    end
  end
end
